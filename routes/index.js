const axios = require('axios');
const sqlite = require('sqlite3').verbose();
const config = require('../credentials.json');
var api = 'http://10pdev.us/api/testresult/defectinfo';
var db = new sqlite.Database('./db/testworthy.db');
db.run('CREATE TABLE IF NOT EXISTS projects(id integer primary key, appId text, projectKey text, userEmail text, tmaKey text)');



export default function routes(app, addon) {
  app.get('/', (req, res) => {
    res.redirect('/atlassian-connect.json');
  });

  app.get('/config-page', addon.authenticate(), (req, res) => {
    var tmaKey = '';
    var projectKey = '';
    var userEmail = '';
    db.each(`SELECT id, projectKey, userEmail, tmaKey FROM projects WHERE appId like '${req.context.hostBaseUrl.replace("https://", "")}'`, [], function (error, row) {

      if (error) {
        res.render(
          'configure-page.hbs',
          {
            title: 'Atlassian Connect',
            tmaKey: tmaKey,
            projectKey: projectKey,
            userEmail: userEmail,
          }
        );
      } else {
        tmaKey = row.tmaKey;
        projectKey = row.projectKey;
        userEmail = row.userEmail;
        res.render(
          'configure-page.hbs',
          {
            title: 'Atlassian Connect',
            tmaKey: tmaKey,
            projectKey: projectKey,
            userEmail: userEmail,
          }
        );
      }
    });
  });
  app.post('/save-config', function (req, res) {
    let data = { status: 200, message: '' };
    db.each(`SELECT id, projectKey,tmaKey,userEmail FROM projects WHERE appId like '${req.body.appId}'`, [], function (error, row) {
      if (error) {
        data.status = 403;
        data.message = error.message;
        res.send(data);
      }
     
      if (typeof row != "undefined") {
     
        if (row.projectKey != req.body.projectKey || row.tmaKey != req.body.tmaKey || row.userEmail != req.body.userEmail) {
            db.run(`UPDATE projects SET projectKey='${req.body.projectKey}', tmaKey='${req.body.tmaKey}', userEmail='${req.body.userEmail}' WHERE appId='${req.body.appId}'`, [], error => {
         
            if (error) {
              data.status = 403;
              data.message = error.message
            }
          })
        }
      } else {
        db.run(`INSERT INTO(appId,tmaKey, projectKey, userEmail) projects  VALUES('${req.body.appId}','${req.body.tmaKey}','${req.body.projectKey}','${req.body.userEmail}')`, [], error => {
          if (error) {
            data.status = 403;
            data.message = error.message;
            res.send(data);
          } else {
            data.message = "insert";
            res.send(data);
          }
        })
      }
    })
    res.send(data);
  });

  app.get('/testworthy', addon.authenticate(), (req, res) => {

    db.each(`SELECT id, tmaKey, userEmail,projectKey FROM projects WHERE appId like '${req.context.hostBaseUrl.replace("https://", "")}'`, [], function (error, row) {
      if (error) {

      } else {
        const opts = {
          headers: {
            "X-TMA-KEY": row.tmaKey,
            "X-USER-EMAIL": row.userEmail,
            "X-PROJECT-KEY": row.projectKey
          }
        }
        axios.get(`http://10pqa.us/api/testresult/defectinfo?jiraTicket=${req.query.issue}`, opts).then(resp => {
          res.render(
            'testworthy.hbs',
            {
              title: 'Atlassian Connect',
              cases: resp.data,
              total: resp.data.length
            });
        })
      }
    });

  });
}
